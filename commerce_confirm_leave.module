<?php

/**
 * @file
 * Contains commerce_confirm_leave.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function commerce_confirm_leave_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the commerce_confirm_leave module.
    case 'help.page.commerce_confirm_leave':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Warns users when they are about to leave the Commerce order process without having completed it.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_alter().
 *
 * @todo use commerce_confirm_leave_form_BASE_FORM_ID_alter
 */
function commerce_confirm_leave_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $setConfirmation = FALSE;

  // Check current route against configuration.
  $routeMatch = \Drupal::routeMatch();
  $routeName = $routeMatch->getRouteName();
  $config = \Drupal::configFactory()->get('commerce_confirm_leave.settings');

  $configuredRoutes = [];
  $routes = $config->get('routes');
  if (isset($routes['cart_page']) && array_key_exists('cart_page', $routes) && $routes['cart_page'] !== 0) {
    $configuredRoutes[] = 'commerce_cart.page';
  }
  if (isset($routes['checkout_form']) && array_key_exists('checkout_form', $routes) && $routes['checkout_form'] !== 0) {
    $configuredRoutes[] = 'commerce_checkout.form';
  }

  if (in_array($routeName, $configuredRoutes)) {
    $setConfirmation = TRUE;
  }

  // Do not display the confirmation message
  // when the order is set as 'complete'.
  // Could be replaced by checking in JS if there is Commerce form on the page.
  if ($routeName === 'commerce_checkout.form' && $routeMatch->getParameter('step') === 'complete') {
    $setConfirmation = FALSE;
  }

  if ($setConfirmation) {
    $form['#attached']['library'][] = 'commerce_confirm_leave/confirm_leave';
    $form['#attached']['drupalSettings']['commerce_confirm_leave'] = [
      'confirmation_message' => $config->get('confirmation_message'),
    ];
  }

}
